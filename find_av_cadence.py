#!/usr/bin/python

import numpy as np
import sys

# Run with python find_av_cadence.py <timfile>
# Prints average cadence
# Will error if "MODE 1" is in the timfile


timfile = sys.argv[1]

mjds = []
with open(timfile, 'r') as this_tim:
    for line in this_tim.readlines():
        fields = line.split()
        if fields[0] != "FORMAT":
            mjds.append(float(fields[2]))

           
mjds_sorted = sorted(mjds) 

intervals = []
for i in range(1, len(mjds_sorted)):
    intervals.append(mjds_sorted[i] - mjds_sorted[i-1])

print("Average cadence is {} days".format(np.mean(intervals)))
