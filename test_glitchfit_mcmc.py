import glitchfit_mcmc
import pytest
import os
import pickle

DATA_DIR = os.path.dirname(__file__) + "/fixtures"

par = DATA_DIR + "/init.par"
tim = DATA_DIR + "/withpn.tim"
priors = DATA_DIR + "/priors.txt"

def get_object():
    priors = DATA_DIR + "/priors.txt"
    obj = glitchfit_mcmc.measureGlitch(par, tim, priors, 50000, 1, 0)
    return obj

def test_prior_dict():
    with open(DATA_DIR + "/prior_dict.p") as result:
        pickled_ref_data = result.read()

    actual = get_object().read_priors(priors)
    pickled_actual = pickle.dumps(actual)
    assert pickled_actual == pickled_ref_data
    
