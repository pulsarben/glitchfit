import numpy as np
import matplotlib.pyplot as plt
print("done")
from matplotlib.ticker import FormatStrFormatter
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

plt.rcParams["figure.figsize"] = [7.5,10.6]

mjdc, f0, f0e, f1, f1e, mjds, mjdf = np.loadtxt("mjd_f0_f1.dat", unpack=True)
mjdr, res, err = np.loadtxt("out.res", unpack=True, usecols=[0,5,6])


glep = 58240
print(glep)

lower = min(mjdc) - glep
upper = max(mjdc) - glep
'''
# Plot residuals 
plt.errorbar(mjdr-glep, res-res[0], yerr=err, marker='.', color='k', ecolor='k', linestyle='None')
plt.ylabel("Residual (s)")
frame = plt.gca()
frame.axes.xaxis.set_ticklabels([])
plt.xlabel("Days since glitch epoch")
plt.axvline("0", linestyle='dashed', color='k')
#plt.show()

# Plot rotation frequency
plt.errorbar(mjdc-glep, f0, yerr=f0e, marker='.', color='k', ecolor='k', linestyle='None')
plt.xlabel("Days since glitch epoch")
plt.ylabel("Rotation frequency (Hz)")
#plt.show()
'''
##
pre_mjds = []
pre_f0s = []
pre_f0es = []
for i in range(0, len(mjdc)):
    if mjdc[i] < glep:
        pre_mjds.append(mjdc[i])
        pre_f0s.append(f0[i])
        pre_f0es.append(1.0/f0e[i])

p = np.polyfit(pre_mjds, pre_f0s, 2, w=pre_f0es)
p = np.poly1d(p)
print(p)

'''
plt.errorbar(mjdc-glep, (f0 - p(mjdc))/1e-06, yerr=f0e/1e-06, marker='.', color='k', ecolor='k', linestyle='None' )
plt.ylabel("Delta F0 [Microhertz]")

plt.savefig("test1.png")
plt.axvline("0", linestyle='dashed', color='k')
#plt.show()
'''
fig, ax = plt.subplots(nrows=4, ncols=1)

plt.subplot(411)

xlocs = [-300, -200, -100, 0, 100, 200, 300, 400, 500]



plt.errorbar(mjdr-glep, (res-res[0])/1e-03, yerr=err/1e-03, marker='.', color='k', ecolor='k', linestyle='None')
#plt.xlim([-12, 19])
plt.xlim([lower, upper])
plt.ylabel("Residual (ms)", fontsize=20)
plt.axvline("0", linestyle='dashed', color='k')
frame = plt.gca()
frame.axes.xaxis.set_ticklabels([])
plt.xticks(xlocs)

#Plot rotation frequency
plt.subplot(412)
plt.errorbar(mjdc-glep, (f0-f0[0])/1e-06, yerr=f0e/1e-06, marker='.', color='k', ecolor='k', linestyle='None')
#plt.xlim([-9, 16])
#plt.xlim([-12, 19])
plt.xlim([lower, upper])
plt.ylabel(r'$\delta \nu$ ($\mu$Hz)', fontsize=20)
plt.axvline("0", linestyle='dashed', color='k')
frame = plt.gca()
frame.axes.xaxis.set_ticklabels([])
frame.axes.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
yloc = plt.MaxNLocator(5) ; frame.yaxis.set_major_locator(yloc)
plt.xticks(xlocs)

# Plot delta rotation frequency
plt.subplot(413)
plt.errorbar(mjdc-glep, (f0 - p(mjdc))/1e-06, yerr=f0e/1e-06, marker='.', color='k', ecolor='k', linestyle='None' )
#plt.xlim([-9, 16])
#plt.xlim([-12, 19])
plt.xlim([lower, upper])
plt.ylabel(r'$\delta \nu$ ($\mu$Hz)', fontsize=20)
plt.axvline("0", linestyle='dashed', color='k')
frame = plt.gca()
frame.axes.xaxis.set_ticklabels([])
frame.axes.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
yloc = plt.MaxNLocator(5) ; frame.yaxis.set_major_locator(yloc)
plt.xticks(xlocs)

plt.subplot(414)
plt.errorbar(mjdc-glep, (f1 - f1[0])/1e-11, yerr=f1e/1e-11,  marker='.', color='k', ecolor='k', linestyle='None')
#plt.xlim([-9, 16])
#plt.xlim([-12, 19])
plt.xlim([lower, upper])
plt.ylabel(r'$\delta \dot{\nu}$ ($10^{-11}$ Hz s$^{-1}$)', fontsize=20)
plt.axvline("0", linestyle='dashed', color='k')
frame = plt.gca()
frame.axes.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
yloc = plt.MaxNLocator(5) ; frame.yaxis.set_major_locator(yloc)
plt.xticks(xlocs, xlocs)
plt.tight_layout()
plt.subplots_adjust(wspace=0, hspace=0.08)

plt.savefig("timing_1.pdf", format='pdf', dpi=400)

plt.show()
